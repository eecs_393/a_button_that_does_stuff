from src.Client import *

# makes a new client with a uuid, network connection, and pressqueue
n = NetworkConnection()
pq = PressQueue()
c = Client('1234-ABCD-5678-EFGH', n, pq)

# button press registration process
# this will be event driven with a hardware button this line will represent a new press event
c.network.connect() # done first so location may be detected
c.press_queue.add(Press()) # adds to the queue
while not c.press_queue.flush(c.network, c.uuid): # attempt POST requests
    print ('still trying') # plan is to adapt this so it tries another network if this fails... probably not worth continuing to try repeatedly.
print ('wahoo! it worked')
c.network.disconnect()
