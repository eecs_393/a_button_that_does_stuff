import requests
import json
import time, datetime
import os
from queue import Queue
from wifi import Cell, Scheme


class Client(object):
    """Represents a client object that contains a UUID"""

    def __init__(self, uuid: str, network, press_queue):
        self.uuid = uuid
        self.network = network
        self.press_queue = press_queue

    def get_uuid(self):
        """Returns a hexadecimal representation of the Client’s 32 byte UUID."""
        return str(self.uuid)


class NetworkConnection(object):
    """ A Network Connection with the following responsibilities:
    Upon a press, obtain the geographic location of the Client if possible.
    Attempt to connect to an open WiFi network until successful."""
        

    def connect(self):
        if (self.is_connected()):
            return True
        """Attempts to establish a WiFi connection to an open internet connected WiFi network."""
        available_networks = Cell.all('wlan0')
        for ntwrk in available_networks:
            ###for testing only. this is my hotspot###
            if ntwrk.ssid == 'Verizon-SM-G935V-566C':
                print (ntwrk.ssid) # debug
                scheme = Scheme.for_cell('wlan0', 'open_network', ntwrk, passkey='chob513(')
                try:
                    scheme.save()
                except:
                    pass
                try:
                    scheme.activate()
                except:
                    return False
                return True
            ###------------------------------------###

            if ntwrk.encryption_type is None:
                print (ntwrk.ssid) # debug
                scheme = Scheme.for_cell('wlan0', 'open_network', ntwrk, passkey=None)
                try:
                    scheme.save()
                except:
                    pass
                try:
                    scheme.activate()
                except:
                    return False
                return True
        return False

    def disconnect(self):
        """Disconnects any active WiFi connection."""
        os.system('sudo ifdown wlan0')
        self.is_connected = False

    def is_connected(self):
        """Returns true if  connected to a WiFi network, False otherwise"""
        try:
            r = requests.get('http://www.google.com/', timeout = 3)
            return True
        except requests.ConnectionError:
            print ('no internet')
        return False

class Press(object):
    """ A button press with the following responsibilities:
    Register hardware button presses by a user."""

    def __init__(self):
        self.timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

        send_url = 'http://freegeoip.net/json'
        r = requests.get(send_url)
        j = json.loads(r.text)
        lat = j['latitude']
        lon = j['longitude']
        self.location = (lat, lon)

    def get_timestamp(self):
        return self.timestamp

    def get_location(self):
        return self.location


class PressQueue(Queue):
    """
    A Queue of Presses with the following responsibilities:
    Store recorded presses with the obtained location and a timestamp.
    Make a POST request to Action Management for each stored press that includes the
    previously obtained location, timestamp of the press, and the UUID of the Client."""

    def __init__(self):
        Queue.__init__(self, maxsize=0)

    def add(self, press: Press):
        self.put_nowait(press)

    def remove(self):
        return self.get_nowait()

    def length(self):
        return self.qsize()

    def flush(self, network, uuid: str):
        """
        Makes a POST request to Action Management for each Press in PressQueue
        return: True if successful, False otherwise
        """
        while self.length() > 0:
            try:
                if (network.is_connected()):
                    p = self.remove()                  
                    u = 'http://127.0.0.1:8000/intake/' + uuid + ';' + p.get_timestamp() + ';' + p.get_location()
                    r = requests.post(u)
                    # check to ensure it went through
                    # else throw exception
            except:
                return False
        return True
