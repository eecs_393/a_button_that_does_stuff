from django.db import models
from config.models import UserPreset, UserProfile
from django.contrib.auth.models import User
from actionmanager.ActionRequest import *
from actionmanager.Logger import *
from event_enums import EventType, EventStatus
from integrations.Integration import Integration


class ActionPerformer:
    def perform_action(request):
        userprofile = UserProfile.objects.get(uuid=request.id)
        user = User.objects.get(id=userprofile.user_id)
        user_presets = UserPreset.objects.filter(user_id=user.id)
        success_states = []
        for user_preset in user_presets:
            if (ActionPerformer.check_time_range(request, user_preset)):
                state = Integration.execute_integration(user_preset)
                success_states.append(state)
        Logger.write(uuid=request.id, request_timestamp=request.time, event_type=EventType.REQUEST_PROCESSED,
                     status=EventStatus(success_states[0]))

    #def simulate_action(request):
    #    userprofile = UserProfile.objects.get(uuid=request.id)
     #   user = User.objects.get(id=userprofile.user_id)
      #  user_presets = UserPreset.objects.filter(user_id=user.id)
       # success_states = []
        #for user_preset in user_presets:
        #    if (ActionPerformer.check_time_range(request, user_preset)):
        #        state = EventStatus.SUCCESS
        #        success_states.append(state)
        #Logger.write(uuid=request.id, request_timestamp=request.time, event_type=EventType.REQUEST_PROCESSED,
         #            status=EventStatus(success_states[0]))

    # print("UUID is " + request.id + " Action type is " + user_action)
    def check_time_range(request, user_preset):
        return (user_preset.start_time <= request.time.time()) & (user_preset.end_time >= request.time.time())
