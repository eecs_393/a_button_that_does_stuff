from actionmanager.Logger import Logger
from actionmanager.ActionPerformer import ActionPerformer
from event_enums import *
from config.models import *
from pytz import timezone
from django.utils import timezone


def _tw(time):
    return datetime.datetime.combine(datetime.date.today(), time)


class Validator:
    def __init__(self, performer):
        self.performer = performer

    def single_action_test(uuid, timestamp):
        valid_entries = LogEntry.objects.filter(
            uuid=uuid,
            event_status=EventStatus.VALID.value
        )
        if (len(valid_entries) > 0):
            latest_valid = valid_entries.latest('timestamp')
            # timestamp = timestamp.replace(tzinfo = latest_valid.timestamp.tzinfo)
            return latest_valid.timestamp < timestamp
        return True

    def freq_test(uuid, timestamp, frequency):
        valid_entries = LogEntry.objects.filter(
            uuid=uuid,
            event_status=EventStatus.VALID.value
        )
        if (len(valid_entries) > 0):
            latest_valid = valid_entries.latest('request_timestamp')
            # timestamp = timestamp.replace(tzinfo = latest_valid.request_timestamp.tzinfo)
            time_delta = latest_valid.request_timestamp - timestamp
            print(uuid)
            print(abs(time_delta))
            print(timestamp)
            print(latest_valid.request_timestamp)
            print(frequency)
            return frequency < abs(time_delta)
        return True

    def validate(request):
        """TODO get prefs, logs"""
        try:
            profile = UserProfile.objects.get(uuid=request.id)
            user = User.objects.get(id=profile.user_id)
        except User.DoesNotExist as e:
            return EventStatus.NO_SUCH_ACCOUNT

        current_time = timezone.now()
        request_time = request.time
        time_delta = current_time - request_time

        single_passed = Validator.single_action_test(request.id, request.time)
        freq_passed = Validator.freq_test(request.id, request.time, profile.frequency)

        if (profile.has_timeout & (profile.timeout < time_delta)):
            Logger.write(
                uuid=request.id,
                request_timestamp=request.time,
                event_type=EventType.REQUEST_RECIEVED,
                status=EventStatus.INVALID_TIMEOUT
            )
            return EventStatus.INVALID_TIMEOUT
        elif (profile.single_action and not single_passed):
            Logger.write(
                uuid=request.id,
                request_timestamp=request.time,
                event_type=EventType.REQUEST_RECIEVED,
                status=EventStatus.INVALID_SINGLE_ACTION_VIOLATION
            )
            return EventStatus.INVALID_SINGLE_ACTION_VIOLATION
        elif (profile.has_frequency and not freq_passed):
            Logger.write(
                uuid=request.id,
                request_timestamp=request.time,
                event_type=EventType.REQUEST_RECIEVED,
                status=EventStatus.INVALID_FREQ_VIOLATION
            )
            return EventStatus.INVALID_FREQ_VIOLATION
        else:
            Logger.write(
                uuid=request.id,
                request_timestamp=request.time,
                event_type=EventType.REQUEST_RECIEVED,
                status=EventStatus.VALID
            )
            ActionPerformer.perform_action(request)
            return EventStatus.VALID
