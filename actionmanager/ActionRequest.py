import datetime
from pytz import timezone
import pytz
from django.utils import timezone

class ActionRequest:
    """A wrapper class for information about a button press"""

    def __init__(self, uuid, time, location=None):
        self.id = uuid
        self.time = time
        self.location = location

    def create_from_string(request_str):
        components = request_str.split(';')
        id = components[0]
        time = datetime.datetime.strptime(components[1], '%Y-%m-%d %H:%M:%S %z')
        if (len(components) == 3):
            loc = components[2]
            return ActionRequest(id, time, loc)
        else:
            return ActionRequest(id, time)
