from config.models import LogEntry
from django.contrib.auth.models import User
from config.models import UserProfile
from config.models import UserPreset
import datetime


class Logger:
    def write(uuid, request_timestamp, event_type, status, additional_message=""):
        update = LogEntry(uuid=uuid, request_timestamp=request_timestamp, event_type=event_type.value,
                          event_status=status.value)

        # user_presets = UserPreset.objects.get(id=1)
        # use = user_presets.preset
        # print("user preset")
        # print(use)
        # profile.update_hash(update, use)
        update.save()

    def test(uuid, request_timestamp, event_type, status):
        profile = UserProfile.objects.get(uuid=uuid)
        user = User.objects.get(id=profile.user_id)
        use = 'none'
        i = 0
        while i < 16:
            if i < 3:
                myvar = datetime.datetime(request_timestamp.year - i, request_timestamp.month - i,
                                          request_timestamp.day - i)
            elif i < 6:
                myvar = datetime.datetime(request_timestamp.year - i, request_timestamp.month + i,
                                          request_timestamp.day - i)
            elif i < 11:
                myvar = datetime.datetime(request_timestamp.year - i, request_timestamp.month + i - 5,
                                          request_timestamp.day - i)
            else:
                myvar = datetime.datetime(request_timestamp.year - i, request_timestamp.month - i + 12,
                                          request_timestamp.day - i)
            # Final database will store previous entries and require more complex writing
            update = LogEntry(uuid=uuid, request_timestamp=myvar, event_type=event_type.value,
                              event_status=status)
            if i < 3:
                use = 'twitter'
            elif i < 8:
                use = 'uber'
            elif i < 12:
                use = 'ebay'
            else:
                use = 'twilio'
            profile.update_hash(update, use)
            update.save()
            i += 1
