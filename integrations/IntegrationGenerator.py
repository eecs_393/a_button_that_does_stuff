import requests
from integrations.IntegrationParser import Parser
from requests_oauthlib import OAuth1
from requests_oauthlib import OAuth2Session
from config.models import UserPreset
from datetime import datetime
import pdb
import json

class IntegrationGenerator:
    @staticmethod
    def get_api_endpoint(request):
        if request == "default":
            url = "https://api.twitter.com/1.1/statuses/update.json"
        elif request == "twitter":
            url = "https://api.twitter.com/1.1/statuses/update.json"
        else:
            url = ""
        return url

    @staticmethod
    def generate_api_call(preset):
        request = preset.preset or "default"

        if preset.params != "":
            params = json.loads(preset.params)
        else:
            params = {}

        api_endpoint = IntegrationGenerator.get_api_endpoint(request)

        if request == "default":
            # parse params
            token = "3475381516-BE5JiMviHgF8O6EOJaINX8lBmMMp6wi18Fg9mi5"
            token_secret = "NHLKEXxFSExsJmiPTXvqGemcCOx1g3o8kOxxudCsmWCKT"
            con_key = "GwSvrNB0X8wWRTAoShX2RPaIM"
            if 'consumer_key' in params:
                con_key = params['consumer_key']
            con_secret = "zt67trZN2zb8jKhdfsCgJaWfd8Dz7bVFunpqzT8Ev4kxz1iIuO"
            if 'consumer_secret' in params:
                con_secret = params['consumer_secret']
            auth = OAuth1(con_key, con_secret, token, token_secret)

            status = "Button press at " + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            if 'status' in params:
                status = params[status]
            payload = {"status": status}

            r = requests.post(api_endpoint, params=payload, auth=auth)
            return (Parser.success_state(r))

        if request == "twitter":
            # pull the user params here
            token = "3475381516-BE5JiMviHgF8O6EOJaINX8lBmMMp6wi18Fg9mi5"
            if "consumer_key" in params:
                token = params["consumer_key"]
            token_secret = "NHLKEXxFSExsJmiPTXvqGemcCOx1g3o8kOxxudCsmWCKT"
            if "consumer_secret" in params:
                token_secret = params["consumer_secret"]
            con_key = "GwSvrNB0X8wWRTAoShX2RPaIM"
            con_secret = "zt67trZN2zb8jKhdfsCgJaWfd8Dz7bVFunpqzT8Ev4kxz1iIuO"

            auth = OAuth1(con_key, con_secret, token, token_secret)

            status = params['status']
            payload = {"status": status}

            r = requests.post(api_endpoint, params=payload, auth=auth)
            return (Parser.success_state(r))

        if request == "uber":
            client_id = "0JWyNj_6J2mXOKJ99EMaFbqwm67_YyBp"
            client_secret = "K2BOg2hyiUSy8YdnZOB2pNH8Gag4oOLdtxNfvU1G"
            server_token = "b3Q8p1X7VNMGHwZXx6Mc9uNKmuwKo1L_SDdh8H53"

            start_lat = 41.5091260
            if 'start_latitude' in params:
                start_lat = params['start_latitude']

            start_long = -81.6089870
            if 'start_longitude' in params:
                start_longitude = params['start_longitude']

            end_lat = 41.504341
            if 'end_latitude' in params:
                end_lat = params['end_latitude']

            end_long = -81.608384
            if 'end_longitude' in params:
                end_long = params['end_longitude']

            access_token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZXMiOlsiaGlzdG9yeSIsImhpc3RvcnlfbGl0ZSIsInBsYWNlcyIsInByb2ZpbGUiLCJyaWRlX3dpZGdldHMiXSwic3ViIjoiMzBhYTM5ZmItYWM0Yi00NTBhLWFiODItMTdjYTM5ZDk2YzEwIiwiaXNzIjoidWJlci11czEiLCJqdGkiOiI4Nzc0MmViNS0wNWI3LTQ3ZmEtOGI1OC1iYzViMmNhYzBlMTkiLCJleHAiOjE0OTMxNDI2OTgsImlhdCI6MTQ5MDU1MDY5NywidWFjdCI6Im5ZSUE2bnVRVExzTkFMdHVibWR6a015UkY4MTQwSyIsIm5iZiI6MTQ5MDU1MDYwNywiYXVkIjoiMEpXeU5qXzZKMm1YT0tKOTlFTWFGYnF3bTY3X1l5QnAifQ.aaQsTXplwoSl6W39afPjUv4JX8dlPkaISlt1_RGfvoaloS88jGv6eoJXsc7FUcDJ0sUJNN1n6p1liZoETqvibxmkqZ_D4dNUXpdog3HE68KNBI1GF1mo8bwrQ6ndBezGtQVujxfxa0F14Wkhy1Di6p9RbAWzih1-eFvlIdEM-2p-xXgFJwwX9MwdpeKjL8KHlm4oufq3dzUV6Nl8BryPkKggDHADbE90giFO3cguE00c-PppTVtqfHNphmdnmzhMxacuxyDqn634f2VFevVD2uhgmOknOHkii2UJxzOaj3BEAWivM7g5WESg0JQIgsH0GxyCVBv997yQA6Dmw7gG_Q"
            if 'access_token' in params:
                access_token = params['access_token']

            # verify valid access token
            auth_header = {"Authorization": "Bearer %s" % access_token}
            r = requests.get('https://api.uber.com/v1.2/me', headers=auth_header)
            if Parser.success_state(r) == "F":
                return ("F")

            # lookup available rides
            payload = {"latitude": start_lat, "longitude": start_long}
            r = requests.get("https://sandbox-api.uber.com/v1.2/products", params=payload, headers=auth_header)
            product_id = r.json()['products'][0]['product_id']
            if Parser.success_state(r) == "F":
                return ("F")

            # fare estimate
            auth_string = "Bearer %s" % access_token
            payload = {"product_id": product_id, "start_latitude": start_lat, "start_longitude": start_long,
                       "end_latitude": end_lat, "end_longitude": end_long, "Authorization": auth_string}
            r = requests.post("https://sandbox-api.uber.com/v1.2/requests/estimate", params=payload)
            if Parser.success_state(r) == "F":
                return ("F")
            fare_id = r.json()['fare']['fare_id']

            # request ride
            payload = {"fare_id": fare_id, "start_latitude": start_lat, "start_longitude": start_long,
                       "end_latitude": end_lat, "end_longitude": end_long, "Authorization": auth_string}
            r = requests.post("https://sandbox-api.uber.com/v1.2/requests", params=payload)
            return (Parser.success_state(r))

        if request == "twilio":
            # set params
            phone_number = "+16172911690"
            if "phone_number" in params:
                phone_number = params["phone_number"]
            number_from = "+18575989682"
            body = "notification from A Button That Does Stuff"
            if "body" in params:
                body = params["body"]
            payload = {"To": phone_number, "From": number_from, "Body": body}
            token = "c3d3e27569f88927a91360d36f5ed043"
            auth = ("AC18979dc0519791219c72e0a16064d73f", token)
            r = requests.post(
                "https://api.twilio.com/2010-04-01/Accounts/AC18979dc0519791219c72e0a16064d73f/Messages.json",
                data=payload, auth=auth)
            print(r.text)
            return (Parser.success_state(r))

        if request == "ebay":
            # set params
            dev_id = "15e2c261-4543-4c6f-987c-b45a42640f23"
            client_id = "HarryKwo-AButtonT-SBX-008fef0ab-e32bd901"
            client_secret = "SBX-08fef0ab0cb7-350d-45aa-b1a3-e7d5"
            user_token = "v^1.1#i^1#p^3#I^3#r^0#f^0#t^H4sIAAAAAAAAAOVXfWwURRTvtddKA6VRQZGAXhY0COzd7Nddb8Od3NE2nND27JUCTZDM7s61C3u7685s29MESkMAjSHErxhUUgkYY9T4B4IQlRg1xihRoomBENRgFAUxRBONiDh3vbbXqtAPEpt4/2zmzfv6vfd7czOgp6Jy4bbl236t8txQ2tcDeko9Hm4qqKwoXzS9rHR2eQkoUvD09czv8faWnV2CYcaw5WaEbcvEyNedMUws54URxnVM2YJYx7IJMwjLRJVTsYaVMu8Hsu1YxFItg/ElaiMMpwpIgEpIhQiCEKdQqTngs8WKMGE1GBZqVDGscBoSQoDuY+yihIkJNEmE4QEXYoHA8jUtnCRLnCzx/hqOb2N8rcjBumVSFT9govl05bytU5Tr1VOFGCOHUCdMNBGrTzXFErV1jS1LAkW+ooU6pAgkLh6+WmZpyNcKDRddPQzOa8spV1URxkwg2h9huFM5NpDMONLPlxppAHFqUJFEBAROEa5LKestJwPJ1fPISXSNTedVZWQSnWSvVVFaDWUDUklh1UhdJGp9uc99LjT0tI6cCFMXj61dlaprZnypZNKxOnUNaXlSCYIohoVwiIkShGkJkbO+AzpOdmOXZfKgEK7fZ6HYI+Its0xNz5UO+xotEkc0dzSyQmJRhahSk9nkxNIkl1exXqhQyVCY6gUGeumSDjPXXZSh5fDll9fuwwAxhqhwvaghoHBQEIIQAQ7UcFwxM3KzPl52RHMNiiWTAaTALJuBzkZEbAOqiFVpZd0McnRNFkVFDEI1zQp8jcqKmqSy4bCEWB4FUZCHggjS4v+PIIQ4uuISNEiSkRt5qBEmpVo2SlqGrmaZkSr5o6dAiW4cYToIseVAoKury98l+C2nPcADwAXWNKxMqR0oA5lBXf3ayqye56uKqBXWZZK1aTbdlHs0uNnORAVHS0KHZONulq5TyDDoZ4C/wzKMjpT+C1Scgzq5QObsMXUAbd2f47hftTIBC9JxzonW5zP2jUYpoLhZGl9Djt9BULNMIzt6u3aXcrjfenRGmHbD3z+OFMZQxNysj8PBGILqZiflsuVkxwhzuPEYbKCqWq5JxhOuYDoGi7RrpHXDyI3reAIWmY8lTRMaWaKreDDkhKYsZtsJbXJN2fLcybyiy2JjcZcQy2xhU/E1LAA1aZQGUGGRwCtaGHATwq2hTl1F6/VJht10DWNCuBra/xkSnfW9/x2sxkBsQqhqUedkYyknIV7lgxwrSqLAimowzYZrQiqriBIU+SC9xvDChDAvM3R6MrRkJ9uf4HILE6RNDBq9jE4uUPkTpnDAAFUJsYIENNpaCFmFgwKLQpo0WsgjBEVXur9d5wPDX9XRkvyP6/W8BXo9h+nDHIQAyy0Cd1eUrfKWTWOwTpAfQ1NTrG6/DtN+rLeb9NHoIP9GlLWh7pRWeBrOPLp2c9F7vm8dmDX4oq8s46YWPe/BnKGdcq761iouBOjFnJMkTuLbwLyhXS93i3fGF4ffi9f3Ni/+cZ9+oXX1mfnf7n1kFagaVPJ4yku8vZ6S+J07zj/e9FC1vWfrsc1PHXnyF+dN7cFs4wv3wm/Q17u3fDp3TuDn7w8rvudXvFjiSVUvTZZUtR7fH/E+kOjsqz9XCdvAPZ+Jt7ee+LCn/iUiRbcujU2d/xh/46bjU8w/p60Q3k7+PnvdgS3NB2/+7rXGl1vtPdHb7CNPVJ/edU678tEHtWt+254+uLalrdubObbbvrTn9elvpC5LO7+6yZhy8WB49ab2cNnJ5xZcmXtyxokfLtR5ZK7yVPLd/Z3M9nfmdJ7eZT77+cWPrWlHy5Pv19l7F+w/dPSZL/d9cnp2Srj/j50VM5+ed+mQdW7xw2fvuOvAqR3rNmw7f/TirOjlmacWJtJu309LX5kRfHVHfxv/ArDCbVdpEQAA"

            # start session
            endpoint = "https://api.sandbox.ebay.com/buy/order/v1/checkout_session/initiate"
            auth_string = "Bearer %s" % user_token
            payload = {
                "creditCard": {
                    "accountHolderName": params["billing_first"] + " " + params["billing_last"],
                    "billingAddress":
                        {
                            "addressLine1": params["billing_ad_1"],
                            "city": params["billing_city"],
                            "country": params["billing_country"],
                            "firstName": params["billing_first"],
                            "lastName": params["billing_last"],
                            "postalCode": params["billing_zip"],
                            "stateOrProvince": params["billing_state"]
                        },
                    "brand": params["brand"],
                    "cardNumber": params["card_number"],
                    "expireMonth": params["expire_month"],
                    "expireYear": params["expire_year"]
                },
                "lineItemInputs": [
                    {
                        "itemId": "v1|%s|0" % params["item_id"],
                        "quantity": int(params["quantity"])
                    }
                ],
                "shippingAddress": {
                    "addressLine1": params["billing_ad_1"],
                    "city": params["billing_city"],
                    "country": params["billing_country"],
                    "phoneNumber": params["phone"],
                    "postalCode": params["billing_zip"],
                    "stateOrProvince": params["billing_state"],
                    "recipient": params["recipient"],
                }
            }
            auth_header = {"Authorization": "Bearer %s" % user_token}
            r = requests.post(endpoint, params=payload, headers=auth_header)
            if Parser.success_state(r) == "F":
                return ("F")

            # place order
            sessionId = r.json()["checkoutSessionId"]
            endpoint = "https://api.sandbox.ebay.com/buy/order/v1/checkout_session/%s/place_order" % sessionId
            r = requests.post(endpoint, headers=auth_header)
            return (Parser.success_state(r) == "F")
        if request == "slack":
            # set params
            text = "default message"
            if "message" in params:
                text = params["message"]
            r = requests.post(
                params["webhook_url"],
                json={"text": text})
            return (Parser.success_state(r))

        # checkout session
        return ("F")
