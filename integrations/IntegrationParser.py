import requests
from requests_oauthlib import OAuth1


# from event_enums import EventStatus

class Parser:
    """so the api call is actually made in IntegrationGenerator but the parsing of the response and determination of the success state happens here in IntegrationCaller"""

    @staticmethod
    def success_state(request):
        text = request.text
        status_code = request.status_code
        print(status_code)
        if status_code / 100 == 2:
            return "S"  # EventStatus.SUCCESS
        else:
            return "F"  # EventStatus.FAILURE
