import requests
from urllib.parse import *
from requests_oauthlib import OAuth1
from integrations import IntegrationAction
from integrations import IntegrationGenerator
from integrations import IntegrationParser
from config.models import UserPreset
import pdb


class Integration:
    @staticmethod
    def execute_integration(user_action):
        success_state = IntegrationGenerator.IntegrationGenerator.generate_api_call(user_action)
        return success_state

    @staticmethod
    def list_integrations():
        return {
            "twitter": [
                {"name": "consumer_key", "human_name": "Consumer Key"},
                {"name": "consumer_secret", "human_name": "Consumer Key Secret"},
                {"name": "status", "human_name": "Status Content"}
            ], "uber": [
                {"name": "access_token", "human_name": "Access Token"},
                {"name": "end_longitude", "human_name": "End Longitude"},
                {"name": "end_latitude", "human_name": "End Latitude"}
            ], "twilio": [
                {"name": "phone_number", "human_name": "Phone Number"},
                {"name": "body", "human_name": "Text Message Content"}
            ], "ebay": [
                {"name": "client_id", "human_name": "Client ID"},
                {"name": "client_secret", "human_name": "Client Secret"},
                {"name": "user_token", "human_name": "User Token"},
                {"name": "billing_address_line_1", "human_name": "Billing Address Line 1"},
                {"name": "billing_city", "human_name": "Billing City"},
                {"name": "billing_country", "human_name": "Billing Country"},
                {"name": "billing_firstName", "human_name": "Billing First Name"},
                {"name": "billing_lastName", "human_name": "Billing Last Name"},
                {"name": "billing_postalCode", "human_name": "Billing Postal Code"},
                {"name": "billing_stateOrProvince", "human_name": "Billing State or Province"},
                {"name": "brand", "human_name": "Card Brand"},
                {"name": "cardNumber", "human_name": "Card Brand"},
                {"name": "expireMonth", "human_name": "Expiration Month (01-12)"},
                {"name": "expireYear", "human_name": "Expiration Year (Last 2 Digits)"},
                {"name": "itemId", "human_name": "Item ID"},
                {"name": "quantity", "human_name": "Quantity"},
                {"name": "quantity", "human_name": "Quantity"},
                {"name": "shipping_address_line_1", "human_name": "Billing Address Line 1"},
                {"name": "shipping_city", "human_name": "City"},
                {"name": "shipping_country", "human_name": "Country"},
                {"name": "shipping_firstName", "human_name": "First Name"},
                {"name": "shipping_lastName", "human_name": "Last Name"},
                {"name": "shipping_postalCode", "human_name": "Postal Code"},
                {"name": "shipping_stateOrProvince", "human_name": "State or Province"},
            ], "slack": [
                {"name": "webhook_url", "human_name": "Webhook URL:"},
                {"name": "message", "human_name": "Message"}
            ]
        }

    @staticmethod
    def get_auth_url(action, callback_url):  # ="127.0.0.1:8000/config/presets"):
        auth_url = ""
        if action == "twitter":
            # first get request token then get redirect url
            request_token_url = "https://api.twitter.com/oauth/request_token"
            client_key = "GwSvrNB0X8wWRTAoShX2RPaIM"
            client_secret = "zt67trZN2zb8jKhdfsCgJaWfd8Dz7bVFunpqzT8Ev4kxz1iIuO"
            oauth = OAuth1(client_key, client_secret=client_secret)
            r = requests.post(url=request_token_url, params={"oauth_callback": callback_url}, auth=oauth)

            s = r.text
            resource_owner_key = s[s.index("=") + 1:s.index("&")]
            s = s[s.index("&") + 1:]
            resource_owner_secret = s[s.index("=") + 1:s.index("&")]
            # SAVE KEY AND SECRET
            f = open("oauth_keys.txt", "w+")
            f.write(resource_owner_key + "#" + resource_owner_secret)
            f.close()

            auth_endpoint = "https://api.twitter.com/oauth/authorize"
            auth_url = auth_endpoint + "?oauth_token=" + resource_owner_key

        if action == "uber":
            endpoint = "https://login.uber.com/oauth/v2/authorize"
            client_id = "0JWyNj_6J2mXOKJ99EMaFbqwm67_YyBp"
            auth_url = endpoint + "?client_id=" + client_id + "&response_type=code"

        return auth_url

    @staticmethod
    def check_oauth_verifier(oauth_verifier, action):
        if action == "twitter":
            f = open("oauth_keys.txt", "r+")
            r = f.read()
            rs = r.split("#")
            oauth_key = rs[0]
            oauth_secret = rs[1]
            f.close()

            client_key = "GwSvrNB0X8wWRTAoShX2RPaIM"
            client_secret = "zt67trZN2zb8jKhdfsCgJaWfd8Dz7bVFunpqzT8Ev4kxz1iIuO"

            oauth = OAuth1(client_key, client_secret=client_secret, resource_owner_key=oauth_key,
                           resource_owner_secret=oauth_secret, verifier=oauth_verifier)
            access_token_url = "https://api.twitter.com/oauth/access_token"

            r = requests.post(url=access_token_url, auth=oauth)
            credentials = parse_qs(r.content)
            key = credentials.get(b'oauth_token')[0].decode("utf-8")
            secret = credentials.get(b'oauth_token_secret')[0].decode("utf-8")

            f = open("oauth_keys.txt", "w")
            f.write(key + "#" + secret)
            f.close()

    @staticmethod
    def get_last_keys():
        f = open("oauth_keys.txt", "r+")
        r = f.read()
        rs = r.split("#")
        key = rs[0]
        secret = rs[1]
        f.close()

        return key, secret
