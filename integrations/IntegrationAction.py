class IntegrationAction:
	"""Wrapper class to store an integration action"""
	def __init__(self, user_action, parameters):
		self.user_action = user_action
		self.parameters = parameters
