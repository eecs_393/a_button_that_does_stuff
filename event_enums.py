from enum import Enum


class EventType(Enum):
    REQUEST_RECIEVED = 'R'
    REQUEST_PROCESSED = 'P'


class EventStatus(Enum):
    VALID = 'V'
    INVALID_TIMEOUT = 'IT'
    INVALID_FREQ_VIOLATION = 'IFV'
    INVALID_SINGLE_ACTION_VIOLATION = 'ISAV'
    FAILURE = 'F'
    SUCCESS = 'S'
    NO_SUCH_ACCOUNT = 'NA'
