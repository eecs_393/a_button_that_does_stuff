from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^freqchart$', views.freqchart, name='freqchart'),
    url(r'^piechart$', views.piechart, name='piechart'),
    url(r'^barchart$', views.barchart, name='barchart'),
    url(r'^areachart$', views.areachart, name='areachart'),
]