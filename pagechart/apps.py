from django.apps import AppConfig


class PagechartConfig(AppConfig):
    name = 'pagechart'
