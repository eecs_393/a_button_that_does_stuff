from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from datetime import datetime
from config.modules import stats
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import models
from config.models import UserProfile
from django.contrib.auth.models import User
from pagechart.tests import stuff
from actionmanager.Logger import *
from config.modules import index_helper
from pagechart.tests import *


# Create your views here.

@login_required
def index(request):
    return render(request, 'pagechart/base.html', {'auth': True})


@login_required
def freqchart(request):
    return render(request, 'pagechart/freqchart.html', {'auth': True})


@login_required
def piechart(request):
    return render(request, 'pagechart/piechart.html', {'auth': True})


@login_required
def barchart(request):
    return render(request, 'pagechart/barchart.html', {'auth': True})


@login_required
def areachart(request):
    return render(request, 'pagechart/areachart.html', {'auth': True})


@login_required
def freqchart (request):
    years = 31 #max year
    user = request.user
    profile = UserProfile.objects.get(user=user)
    stamps = profile.get_stamps()
    tweet = []
    twilio =[]
    uber = []
    ebay = []
    i = 0
    while  i < len(stamps):
        hash = ((stamps[i][0] +44) * stamps[i][1] *  (stamps[i][2] + 12)) - 1
        utc = [stamps[i][0],stamps[i][1],stamps[i][2]]
        if stamps[i][3] == 0:
            tweet.append([utc,profile.get_hash_val(hash)])
        elif stamps[i][3] == 1:
            twilio.append([utc,profile.get_hash_val(hash)])
        elif stamps[i][3] == 2:
            uber.append([utc,profile.get_hash_val(hash)])
        else:
            ebay.append([utc,profile.get_hash_val(hash)])
        i+= 1
    return render(request,'pagechart/freqchart.html', {'tweet': tweet, 'twilio': twilio, 'uber': uber, 'ebay': ebay})

@login_required   
def piechart (request):
    user = request.user
    context =  UserProfile.objects.get(user=user).type_breakdown
    return render(request,'pagechart/piechart.html', {'context': context})
    
@login_required    
def barchart (request):
    years = 31 #max year
    user = request.user
    profile = UserProfile.objects.get(user=user)
    stamps = profile.get_stamps()
    tweet = []
    twilio =[]
    uber = []
    ebay = []
    i = 0
    while  i < len(stamps):
        hash = ((stamps[i][0] +44) * stamps[i][1] *  (stamps[i][2] + 12)) - 1
        utc = [stamps[i][0],stamps[i][1],stamps[i][2]]
        if stamps[i][3] == 0:
            tweet.append([utc,profile.get_hash_val(hash)])
        elif stamps[i][3] == 1:
            twilio.append([utc,profile.get_hash_val(hash)])
        elif stamps[i][3] == 2:
            uber.append([utc,profile.get_hash_val(hash)])
        else:
            ebay.append([utc,profile.get_hash_val(hash)])
        i+= 1
    
    return render(request,'pagechart/barchart.html', {'tweet': tweet, 'twilio': twilio, 'uber': uber, 'ebay': ebay})
@login_required    
def areachart (request):
    
    years = 31 #max year
    user = request.user
    profile = UserProfile.objects.get(user=user)
    stamps = profile.get_stamps()
    tweet = []
    twilio =[]
    uber = []
    ebay = []
    i = 0
    while  i < len(stamps):
        hash = ((stamps[i][0] +44) * stamps[i][1] *  (stamps[i][2] + 12)) - 1
        utc = [stamps[i][0],stamps[i][1],stamps[i][2]]
        if stamps[i][3] == 0:
            tweet.append([utc,profile.get_hash_val(hash)])
        elif stamps[i][3] == 1:
            twilio.append([utc,profile.get_hash_val(hash)])
        elif stamps[i][3] == 2:
            uber.append([utc,profile.get_hash_val(hash)])
        else:
            ebay.append([utc,profile.get_hash_val(hash)])
        i+= 1
    return render(request,'pagechart/areachart.html', {'tweet': tweet, 'twilio': twilio, 'uber': uber, 'ebay': ebay})


