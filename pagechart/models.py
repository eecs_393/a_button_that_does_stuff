from __future__ import unicode_literals
import datetime
from django.db import models
from django.contrib.auth.models import User

def get_user(request):
    return UserProfile.objects.get(uuid=request.id)
   
        