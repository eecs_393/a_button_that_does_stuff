# A Button That Does Stuff

## Getting Started

1. Install dependencies: `pip3 install -r requirements.txt`
1. Open the shell: `python3 manage.py shell`
1. Import models: `from config.models import *`
1. Create a UserProfile with some UUID: `UserProfile(uuid=43).save()`
1. Leave the shell `^D`
1. Run migrations: `python3 manage.py makemigrations; python3 manage.py migrate`
1. Run the server `python3 manage.py runserver`
1. Hit the intake endpoint from the browser: `http://localhost:8080/intake/<uuid>;<time>;<location>`