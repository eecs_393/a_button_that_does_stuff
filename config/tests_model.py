from django.test import TestCase
from config.models import *
from actionmanager.ActionPerformer import *
from actionmanager.Validator import *
from actionmanager.ActionRequest import *
from intake.views import *
from event_enums import *
import intake
import datetime
from pytz import timezone
import pytz
from django.utils import timezone
# Create your tests here.

class ActionManagerTestCase(TestCase):
	def setUp(self):
		userA = User(username='A-san',email='test@example.com',password='password')
		userA.save()
		UserProfile(user = userA, uuid = 1).save()
		UserPreset(user = userA, params="{  \"consumer_key\": \"GwSvrNB0X8wWRTAoShX2RPaIM\",  \"consumer_secret\": \"zt67trZN2zb8jKhdfsCgJaWfd8Dz7bVFunpqzT8Ev4kxz1iIuO\",  \"status\": \"<text of the status you want to post>\" }").save()
		
		userB = User(username='B-san',email='test@example.com',password='password')
		userB.save()
		UserProfile(user = userB, uuid = 2, has_timeout = True, timeout = datetime.timedelta(minutes = 5)).save()
		UserPreset(user = userB, params="{  \"consumer_key\": \"GwSvrNB0X8wWRTAoShX2RPaIM\",  \"consumer_secret\": \"zt67trZN2zb8jKhdfsCgJaWfd8Dz7bVFunpqzT8Ev4kxz1iIuO\",  \"status\": \"<text of the status you want to post>\" }").save()
		
		
		userC = User(username='C-san',email='test@example.com',password='password')
		userC.save()
		UserProfile(user = userC, uuid = 3, single_action = True).save()
		UserPreset(user = userC, params="{  \"consumer_key\": \"GwSvrNB0X8wWRTAoShX2RPaIM\",  \"consumer_secret\": \"zt67trZN2zb8jKhdfsCgJaWfd8Dz7bVFunpqzT8Ev4kxz1iIuO\",  \"status\": \"<text of the status you want to post>\" }").save()
		
		
		userD = User(username='D-san',email='test@example.com',password='password')
		userD.save()
		UserProfile(user = userD, uuid = 4, has_frequency = True, frequency = datetime.timedelta(seconds = 30)).save()
		UserPreset(user = userD, params="{  \"consumer_key\": \"GwSvrNB0X8wWRTAoShX2RPaIM\",  \"consumer_secret\": \"zt67trZN2zb8jKhdfsCgJaWfd8Dz7bVFunpqzT8Ev4kxz1iIuO\",  \"status\": \"<text of the status you want to post>\" }").save()
	#tests that responses are interpreted as valid when they do not violate any parameter
	def test_simple_case(self):
		first_timestamp = datetime.datetime.strftime(timezone.localtime(timezone.now()), '%Y-%m-%d %H:%M:%S %z')
		print(first_timestamp)
		simple_request_1 = ActionRequest.create_from_string("1;"+first_timestamp+";")
		simple_request_2 = ActionRequest.create_from_string("2;"+first_timestamp+";")
		simple_request_3 = ActionRequest.create_from_string("3;"+first_timestamp+";")
		simple_request_4 = ActionRequest.create_from_string("4;"+first_timestamp+";")
		self.assertEqual(Validator.validate(simple_request_1),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_2),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_3),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_4),EventStatus.VALID)
		#simple_response_1 = intake.views.recieve(simple_request_1)
		#self.assertEqual(simple_response_1.value(), 'Confirm reciept of message, valid')
		#simple_response_2 = intake.views.recieve(simple_request_2)
		#self.assertEqual(simple_response_2.value(), 'Confirm reciept of message, valid')
		#simple_response_3 = intake.views.recieve(simple_request_3)
		#self.assertEqual(simple_response_3.value(), 'Confirm reciept of message, valid')
		#simple_response_4 = intake.views.recieve(simple_request_4)
		#self.assertEqual(simple_response_4.value(), 'Confirm reciept of message, valid')
	
	def test_timeout_case(self):
		first_timestamp = datetime.datetime.strftime(timezone.localtime(timezone.now())-datetime.timedelta(minutes = 5), '%Y-%m-%d %H:%M:%S %z')
		timeout_request_1 = ActionRequest.create_from_string("1;"+first_timestamp+";")
		timeout_request_2 = ActionRequest.create_from_string("2;"+first_timestamp+";")
		timeout_request_3 = ActionRequest.create_from_string("3;"+first_timestamp+";")
		timeout_request_4 = ActionRequest.create_from_string("4;"+first_timestamp+";")
		self.assertEqual(Validator.validate(timeout_request_1),EventStatus.VALID)
		self.assertEqual(Validator.validate(timeout_request_2),EventStatus.INVALID_TIMEOUT)
		self.assertEqual(Validator.validate(timeout_request_3),EventStatus.VALID)
		self.assertEqual(Validator.validate(timeout_request_4),EventStatus.VALID)
		#timeout_response_1 = intake.views.recieve(timeout_request_1)
		#self.assertEqual(simple_response_1.value(), 'Confirm reciept of message, valid')
		#timeout_response_2 = intake.views.recieve(timeout_request_2)
		#self.assertEqual(timeout_response_2.value(), 'Confirm reciept of message, invalid due to timeout')
		#timeout_response_3 = intake.views.recieve(timeout_request_3)
		#self.assertEqual(timeout_response_3.value(), 'Confirm reciept of message, valid')
		#timeout_response_4 = intake.views.recieve(timeout_request_4)
		#self.assertEqual(timeout_response_4.value(), 'Confirm reciept of message, valid')
	def test_single_action_case(self):
		first_timestamp = datetime.datetime.strftime(timezone.localtime(timezone.now())-datetime.timedelta(seconds = 35), '%Y-%m-%d %H:%M:%S %z')
		second_timestamp = datetime.datetime.strftime(timezone.localtime(timezone.now())-datetime.timedelta(seconds = 1), '%Y-%m-%d %H:%M:%S %z')
		simple_request_1 = ActionRequest.create_from_string("1;"+first_timestamp+";")
		simple_request_2 = ActionRequest.create_from_string("2;"+first_timestamp+";")
		simple_request_3 = ActionRequest.create_from_string("3;"+first_timestamp+";")
		simple_request_4 = ActionRequest.create_from_string("4;"+first_timestamp+";")
		self.assertEqual(Validator.validate(simple_request_1),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_2),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_3),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_4),EventStatus.VALID)
		single_action_request_1 = ActionRequest.create_from_string("1;"+second_timestamp+";")
		single_action_request_2 = ActionRequest.create_from_string("2;"+second_timestamp+";")
		single_action_request_3 = ActionRequest.create_from_string("3;"+second_timestamp+";")
		single_action_request_4 = ActionRequest.create_from_string("4;"+second_timestamp+";")
		self.assertEqual(Validator.validate(single_action_request_1),EventStatus.VALID)
		self.assertEqual(Validator.validate(single_action_request_2),EventStatus.VALID)
		self.assertEqual(Validator.validate(single_action_request_3),EventStatus.INVALID_SINGLE_ACTION_VIOLATION)
		self.assertEqual(Validator.validate(single_action_request_4),EventStatus.VALID)
	def test_frequency_case(self):
		first_timestamp = datetime.datetime.strftime(timezone.localtime(timezone.now()), '%Y-%m-%d %H:%M:%S %z')
		second_timestamp = datetime.datetime.strftime(timezone.localtime(timezone.now())+datetime.timedelta(seconds = 10), '%Y-%m-%d %H:%M:%S %z')
		simple_request_1 = ActionRequest.create_from_string("1;"+first_timestamp+";")
		simple_request_2 = ActionRequest.create_from_string("2;"+first_timestamp+";")
		simple_request_3 = ActionRequest.create_from_string("3;"+first_timestamp+";")
		simple_request_4 = ActionRequest.create_from_string("4;"+first_timestamp+";")
		self.assertEqual(Validator.validate(simple_request_1),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_2),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_3),EventStatus.VALID)
		self.assertEqual(Validator.validate(simple_request_4),EventStatus.VALID)
		single_action_request_1 = ActionRequest.create_from_string("1;"+second_timestamp+";")
		single_action_request_2 = ActionRequest.create_from_string("2;"+second_timestamp+";")
		single_action_request_3 = ActionRequest.create_from_string("3;"+second_timestamp+";")
		single_action_request_4 = ActionRequest.create_from_string("4;"+second_timestamp+";")
		self.assertEqual(Validator.validate(single_action_request_1),EventStatus.VALID)
		self.assertEqual(Validator.validate(single_action_request_2),EventStatus.VALID)
		self.assertEqual(Validator.validate(single_action_request_3),EventStatus.VALID)
		self.assertEqual(Validator.validate(single_action_request_4),EventStatus.INVALID_FREQ_VIOLATION)