from django.test import TestCase, Client
from django.contrib.auth.models import User
from config.models import *
from actionmanager.ActionPerformer import *
from actionmanager.Validator import *
from actionmanager.ActionRequest import *
from intake.views import *
from event_enums import *
import intake
import datetime
import json

import pdb


# Create your tests here.

class ConfigTestCase(TestCase):
    def setUpTestData():
        user = User.objects.create(username='test', email='test@example.com')
        user.set_password('password')
        user.save()

    def test_login_page(self):
        self.client.logout()
        response = self.client.get('/login', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Login')

    def test_home_page(self):
        self.client.login(username='test', password='password')
        response = self.client.get('/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'Login')
        self.assertContains(response, 'My actions')

    def test_logout(self):
        self.client.login(username='test', password='password')
        response = self.client.get('/logout', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Login')

