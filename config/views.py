from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import login

from config.modules import index_helper
from config.modules.onboarding import *
from config.modules.presets import *
from config.modules.settings_rsrc import SettingsForm
from config.models import *

from django.contrib.auth.decorators import login_required

from integrations.Integration import Integration

# Display statistics page
@login_required
def stats(request):
    return render(request, 'config/stats.html', {'auth': True})


# Allow user to manage button actions
@login_required
def presets(request):
    # this line will get the current user eventually
    user = request.user
    if request.method == 'POST':
        if 'add' in request.POST:
            add_preset(request.POST['add'], user)
        elif 'update' in request.POST:
            params = generate_update_params(request.POST)
            update_preset(request.POST['update'], user, params)
        elif 'remove' in request.POST:
            remove_preset(request.POST['remove'], user)
    elif "oauth_token" in request.GET:
        Integration.check_oauth_verifier(request.GET["oauth_verifier"], "twitter")
    user_presets = UserPreset.objects.filter(user=user)
    context = {
        'all_presets': Integration.list_integrations(),
        'user_presets': user_presets_with_params(user_presets),
        'user_id': request.user.id,
        'auth': True
    }
    return render(request, 'config/presets.html', context)


@login_required
def twitter(request):
    callback_url = "http://" + request.get_host() + "/config/presets?test"
    return redirect(Integration.get_auth_url('twitter', callback_url))


@login_required
def uber(request):
    callback_url = "http://" + request.get_host() + "/config/presets"
    return redirect(Integration.get_auth_url('uber', callback_url))

# Allow user to change username and password
@login_required
def settings(request):
    user = request.user
    profile = user.userprofile

    if request.method == 'POST':
        form = SettingsForm(request.POST)
        if form.is_valid():
            user.email = form.cleaned_data['email']

            pw = form.cleaned_data['password']
            pwc = form.cleaned_data['password_confirmation']
            if pw and pwc and pw == pwc:
                user.set_password(form.cleaned_data['password'])

            profile.has_timeout = form.cleaned_data['has_timeout']
            profile.timeout = form.cleaned_data['timeout']
            profile.has_frequency = form.cleaned_data['has_frequency']
            profile.frequency = form.cleaned_data['frequency']
            profile.single_action = form.cleaned_data['single_action']

            user.save()
            profile.save()
    else:
        form = SettingsForm(initial={
            'email': user.email,
            'has_timeout': profile.has_timeout,
            'timeout': profile.timeout,
            'has_frequency': profile.has_frequency,
            'frequency': profile.frequency
        })
    return render(request, 'config/settings.html', {'form': form, 'auth': True})



# Set up information for new users
def signup(request):
    if request.method == 'POST':
        form = OnboardingForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
                form.cleaned_data['username'],
                form.cleaned_data['email'],
                form.cleaned_data['password']
            )
            user.save()
            userprofile = UserProfile(
                uuid=int(form.cleaned_data['button_code']),
                single_action=False,
                timeout=datetime.timedelta(),
                frequency=datetime.timedelta(),
                has_frequency=False,
                has_timeout=False,
                parameters="",
                user_id=user.id,
                id=UserProfile.objects.count() + 1
            )
            userprofile.save()
            login(request, user)
            return HttpResponseRedirect('/config/presets')
    else:
        form = OnboardingForm()
    return render(request, 'config/signup.html', {'form': form, 'auth': False})
