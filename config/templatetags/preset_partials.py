from django import template

register = template.Library()


@register.inclusion_tag('config/forms/twitter.html')
def show_twitter(p):
    return {'p': p}


@register.inclusion_tag('config/forms/uber.html')
def show_uber(p):
    return {'p': p}

@register.inclusion_tag('config/forms/twilio.html')
def show_twilio(p):
    return {'p': p}

@register.inclusion_tag('config/forms/ebay.html')
def show_ebay(p):
    return {'p': p}

@register.inclusion_tag('config/forms/slack.html')
def show_slack(p):
    return {'p': p}