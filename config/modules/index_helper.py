from datetime import datetime as dt
from datetime import timedelta
from config.models import *
import re


# TODO: right now this always gets called twice and so hits the database twice
def past_day_presses():
    timestamp_from = dt.now() - timedelta(days=1)
    timestamp_to = dt.now()
    return LogEntry.objects.filter(request_timestamp__range=(timestamp_from, timestamp_to))


def press_count():
    return past_day_presses().count()


def current_user():
    return 'Jonah'


def all_presses():
    return [[p.uuid, p.timestamp.strftime('%H:%M:%S')] for p in past_day_presses()]

def time_thing():
    i = 1
    time_arr = []
    while i <= len(LogEntry.objects.all()):
        date =  LogEntry.objects.get(id = i).timestamp
        date_list = [date.year, date.month, date.day]
        time_arr.append(date_list)    
        i += 1
    
    return  time_arr;

