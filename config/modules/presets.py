from config.models import *
from integrations.Integration import Integration

import json


def get_params(name):
    param_options = Integration.list_integrations()
    if name in param_options:
        return param_options[name]
    else:
        return []


def add_preset(name, user):
    UserPreset(user=user, preset=name).save()


def remove_preset(id, user):
    preset = UserPreset.objects.get(id=id)
    if preset.user == user:
        preset.delete()


def update_preset(id, user, params):
    preset = UserPreset.objects.get(id=id)
    key, secret = Integration.get_last_keys()
    if preset.user == user:
        if preset.preset == "twitter":
            preset.params = json.dumps({"consumer_key":key, "status":params["status"], "consumer_secret":secret})
        else:
            preset.params = json.dumps(params)
        preset.save()


def generate_update_params(post):
    params = {}
    preset = UserPreset.objects.get(id=int(post['update']))
    for param in Integration.list_integrations()[preset.preset]:
        params[param['name']] = post[param['name']]
    return params


def user_presets_with_params(user_presets):
    return_array = []
    for p in user_presets:
        preset_data = {
            'preset': p,
            'params': json.loads(p.params or "{}")
        }
        params = get_params(p.preset)
        for q in params:
            if q['name'] not in preset_data['params']:
                preset_data['params'][q['name']] = ''
        return_array.append(preset_data)
    return return_array
