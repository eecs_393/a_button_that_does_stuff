from django import forms


class SettingsForm(forms.Form):
    email = forms.EmailField(label='Email', required=False)
    password = forms.CharField(label='Password', widget=forms.PasswordInput, required=False)
    password_confirmation = forms.CharField(label='Password', widget=forms.PasswordInput, required=False)
    button_code = forms.CharField(label='Button code', required=False)
    has_timeout = forms.BooleanField(label='Enable timeout', required=False)
    timeout = forms.DurationField(label='Timeout duration', required=False)
    has_frequency = forms.BooleanField(label='Enable rate limiting', required=False)
    frequency = forms.DurationField(label='Minimum time between requests', required=False)
    single_action = forms.BooleanField(label='Enable single action restriction', required=False)
