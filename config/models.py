from __future__ import unicode_literals
import datetime
from django.db import models
from django.contrib.auth.models import User
from pytz import timezone
import pytz
from django.utils import timezone


# Create your models here.
class LogEntry(models.Model):
    EVENT_TYPE = {
        ('R', 'Request Recieved'),
        ('P', 'Request Processed'),
    }
    EVENT_STATUS = {
        ('S', 'Success'),
        ('F', 'Failure'),
        ('IT', 'Invalid due to timeout'),
        ('IFV', 'Invalid due to frequency violation'),
        ('ISAV', 'Invalid due to single-action violation'),
        ('V', 'Valid press'),
    }
    uuid = models.IntegerField(primary_key=False)
    request_timestamp = models.DateTimeField()
    timestamp = models.DateTimeField(auto_now=True)
    event_type = models.CharField(max_length=1, choices=EVENT_TYPE)
    event_status = models.CharField(max_length=4, choices=EVENT_STATUS)
    additional_message = models.TextField()

    def __str__(self):
        return self.additional_message

    class Meta:
        unique_together = ('uuid', 'timestamp',)


''


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    uuid = models.IntegerField()
    single_action = models.BooleanField(default=False)
    has_timeout = models.BooleanField(default=False)
    has_frequency = models.BooleanField(default=False)
    timeout = models.DurationField(default=datetime.timedelta())
    frequency = models.DurationField(default=datetime.timedelta())
    action = models.TextField(default="default")
    parameters = models.TextField(default="")


class UserPreset(models.Model):
    user = models.ForeignKey(User)
    preset = models.TextField(default="twitter")
    params = models.TextField(default="")
    start_time = models.TimeField(default=datetime.time.min)
    end_time = models.TimeField(default=datetime.time.max)
