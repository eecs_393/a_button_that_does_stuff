from django.shortcuts import render
from django.http import HttpResponse
from actionmanager.Validator import *
from actionmanager.ActionRequest import *
from django.views.decorators.csrf import csrf_exempt
from event_enums import EventStatus
from pytz import timezone
import pytz


def index(request):
    return HttpResponse('Intake index')

@csrf_exempt
def recieve(request):
    intake_url = '/intake/'
    reqstr = request.path_info
    if (reqstr.startswith(intake_url)):
        reqstr = reqstr.replace(intake_url, "", 1)
        req = ActionRequest.create_from_string(reqstr)
        status = Validator.validate(req)
        if (status == EventStatus.VALID):
            return HttpResponse('Confirm reciept of message, valid')
        elif (status == EventStatus.INVALID_TIMEOUT):
            return HttpResponse('Confirm reciept of message, invalid due to timeout')
        elif (status == EventStatus.INVALID_SINGLE_ACTION_VIOLATION):
            return HttpResponse('Confirm reciept of message, invalid due to single action violation')
        elif (status == EventStatus.INVALID_FREQ_VIOLATION):
            return HttpResponse('Confirm reciept of message, invalid due to frequency violation')
        elif (status == EventStatus.NO_SUCH_ACCOUNT):
            return HttpResponse('No account matching this UUID exists in the database')
        else:
            return HttpResponse('Confirm reciept of message, internal error')
    else:
        return HttpResponse('Confirm reciept of message, invalid format\nRecieved ' + request.path_info)
